/*!
   @file AHT10.h

   @mainpage AHT10 humidity, temperature & pressure sensor

   @section intro_sec Introduction

    Driver for the AHT10 humidity, temperature & pressure sensor

   These sensors use I2C

   @section author fmawic@gmail.com

   Written by Fran "fmawic" M. Alvarez Wic for Sensoryca.

   @section license BSD

   BSD license, all text here must be included in any redistribution.
   See the LICENSE file for details.

*/
#ifndef AHT10_H
#define AHT10_H

#include <Arduino.h>
#include <stdint.h>

typedef enum {
  eAHT10Address_default = 0x38,
  eAHT10Address_Low     = 0x38,
  eAHT10Address_High    = 0x39,
} HUM_SENSOR_T;


class AHT10Class
{
  public:
    AHT10Class();

    bool begin(unsigned char _AHT10_address = eAHT10Address_default);
    float GetHumidity(void);
    float GetTemperature(void);
    float GetDewPoint(void);
    unsigned char readStatus(void);
    void Reset(void);

  private:
    unsigned long readSensor(bool GetDataCmd);
    unsigned char AHT10_address;

};

#endif
