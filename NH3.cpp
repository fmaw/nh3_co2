/*
   cO2.h
   Library for the control of NH3 sensor MQ137

   Copyright (c) 2013 seeed technology inc.
   Author        :   C�novas Espinal, Antonio
   Create Time   :   22/04/2021
   Change Log    :

   The MIT License (MIT)

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/


#include "Arduino.h"
#include "NH3.h"

/**
  Para declarar la instancia que seria el siguiente ejemplo: NH3 NH3(A1, -0.30865, 0.4923, 2.7, 10.00);
  @param analogPin implica el pin de lecutra de la señal analogica
  @param m parametro de la recta
  @param b parametro de la recta
  @param R0 Valor de la resistencia en aire limpio calculada con anterioridad
  @param Rl Valor de la resistencia medida con multimetro entre la salida y gnd (existe un potenciometro)
  
  @note Para el calculo de R0, se necesita aire limpio, y como se conoce el ratio de Rs/R0 del sensor en aire limpio (segun el datasheet es 3.6)
  y se puede calcular Rs segun unos calculos, se obtiene R0. El siguiente algoritmo que puede hacer esto se pone aqui abajo.

  float calculaR0(uint8_t pin, float Rl) {
    float R0_res = 0;
    float res=0;
    float val=0;
    float Rs_air=0;
    float R0=0;
    for (uint32_t x = 0 ; x < 10 ; x++){ 
        val = analogRead(pin);
        res = val * (VCC(3.3) / Resolution(1024));
        Rs_air = ((VCC * Rl) / res) - Rl;
        R0 = Rs_air / 3.6;
    }
    return R0;
  }
  Se lee el valor en LSB en el pin para aire limpio, se pasa a tensión el valor, se calcula la resistencia Rs
  y como se conoce el ratio se calcula la R0 que tiene el sensor. Una vez que se tiene la R0 se puede ejecutar este codigo.  
*/



NH3::NH3(uint8_t analogPin, float m, float b, float R0, float Rl) {
  this->dataNH3.pin = analogPin;
	this->dataNH3.m = m;
	this->dataNH3.b = b;
	this->dataNH3.R0 = R0;
	this->dataNH3.Rl = Rl;
}

/*preparacion del pin como entrada para la lectura*/
void NH3::begin() {
	pinMode(dataNH3.pin, INPUT);
}

/*LECTURA DE LA TENSIÓN EN EL PIN PARA CALCULAR RS Y CONOCIENDO R0 SE SACA EL RATIO PARA OPERAR CON LA CURVA
 *ESTO DEVUELVE UN DOUBLE CON LAS PPM
*/
double NH3::read() {
  int sensorValue = analogRead(dataNH3.pin);
	float sensor_volt = (float)sensorValue * (VCC / RESOLUTION);
	float RS_gas = ((VCC * dataNH3.Rl) / sensor_volt) - dataNH3.Rl;
	float ratio = RS_gas / dataNH3.R0;
	double ppm_log = (log10(ratio) - dataNH3.b) / dataNH3.m; //Get ppm value in linear scale according to the the ratio value
  double ppm = pow(10, ppm_log); //Convert ppm value to log scale
	return ppm;
}
