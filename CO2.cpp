/*
   cO2.h
   Library for the control of c02 sensor v1.2

   Copyright (c) 2013 seeed technology inc.
   Author        :   C�novas Espinal, Antonio
   Create Time   :   22/04/2021
   Change Log    :

   The MIT License (MIT)

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/


#include "Arduino.h"
#include "CO2.h"

/**
  Para declarar la instancia que seria el siguiente ejemplo: CO2 CO2(A1);
  @param analogPin el pin de lectura analogica del microcontrolador donde tendrá la señal del sensor de CO2
  @note Solamente usar esto para la instancia y luego usar la función begin para incializar el sensor.
*/
CO2::CO2(uint8_t analogPin) {
  this->dataCO2.pin = analogPin;
}
/**
  Esta función pone la patilla analogica como entrada para habilitarla a medir.
  @note Usar la función begin para incializar el sensor.
*/
void CO2::begin() {
  pinMode(dataCO2.pin, INPUT);
}

/**
  Esta funcion permite obtener el resultado en ppm.
  @param volts es la salida de la función read.
  @param *pcurve es la curva del fabricante para entender los niveles de tensión y CO2. Esta definida en CO2.h.
  @return devulve las ppm de CO2, en caso de ser menores de 400, devuelve un -1
  @note No editar la funcion ni los parametros de la función.
*/
int32_t CO2::percentage(float volts, float *pcurve) {
  if ((volts / DC_GAIN ) >= ZERO_POINT_VOLTAGE) {
    return -1;
  }
  else {
    return pow(10, ((volts / DC_GAIN) - pcurve[1]) / pcurve[2] + pcurve[0]);
  }
}

/**
  Esta función permite hacer las lecturas analógicas de CO2 y te las devulve en tensión.
  @return devuelve la tensión leida por el microcontrolador
  @note El numero de muestras y los intervalos entre muestras se encuentran en CO2.h mediante un define! Despues pasar la salida por la funcion percentage.
*/
float CO2::read() {
   int i;
  float v = 0;

  for (i = 0; i < READ_SAMPLE_TIMES; i++) {
    v += analogRead(dataCO2.pin);
    delay(READ_SAMPLE_INTERVAL);
  }
  v = (v / READ_SAMPLE_TIMES) * 5 / 1024 ;
  return v;
}
