/*
   NH3.h
   Library for the control of NH3 sensor MQ137

   Copyright (c) 2013 seeed technology inc.
   Author        :   C�novas Espinal, Antonio
   Create Time   :   22/04/2021
   Change Log    :

   The MIT License (MIT)

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/

#ifndef NH3_H_
#define NH3_H_
#include <Arduino.h>

#define VCC 3.307
#define RESOLUTION 1024

/*
FOR MQ137
float m = -0.30865; //Slope
float b = 0.4923; //Y-Intercept
CANAL A0 float R0_a0 = 25.69; //Sensor Resistance in fresh air from previous code
CANAL A1 float R0_a1 = 16.56; //Sensor Resistance in fresh air from previous code
float Rl = 10.0;*/
 


typedef struct dataNH3 {
  float m;
	float b;
	uint8_t pin;
	float R0;
	float Rl;
	double ppm;
} data_NH3;

class NH3 {
  public:
    data_NH3 dataNH3;
    NH3(uint8_t analogPin, float m, float b, float R0, float Rl);	
    void begin();
		double read();
};

#endif
