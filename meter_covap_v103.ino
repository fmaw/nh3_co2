/****************************************
   Codigo para sensores NH3, CO2, AHT10
   y BME280.
   Cada uno cuenta con su propia libreria!
*****************************************/
#include "CO2.h"
#include "NH3.h"
#include "LoRaWAN.h"
#include <Wire.h>
//#include "bme280.h"
#include "AHT10.h"

#define SN 0x0B     /* Device SN identificator */

#define BANDA EU868 /* emitting band of LoRaWAN */
#define tiempoEnvio 5 /* valor en minutos */
#define timeToSend (tiempoEnvio *60 *1000) /* Tiempo de envio en minutos * 60 (paso a segundos) * 1000 (paso a millis) */

#define MUESTRAS 10

#define SEALEVELPRESSURE_HPA (1011.25)


/*
   I2C sensors of temperature & humdity which could be used.
   BME280 --> class:: bme280 bme;
   AHT10 --> class:: AHT10Class aht;
   ...

*/
AHT10Class aht;

/* NH3 and CO2 Sensors class usage */
NH3 NH3(A1, -0.30865, 0.4923, 1.23, 10.00);
CO2 CO2(A0);

/*NO EDITAR LOS PARAMETROS DE LA CURVA, CALCULAR BIEN LA R0 Y MEDIR BIEN LA RL, TODO SE ENCUENTRA EN NH3.CPP*/
//NH3 NH3(A1, -0.30865, 0.4923, 2.7, 10.00);
//NH3 NH3(A1, -0.30865, 0.4923, 6.85, 10.00);  //R0 con 5 Vdc de fuente


float temp = 0x00;
float hum = 0x00;
float pres = 0x00;
float alt = 0x00;


/*RECOMENDACIÓN DE DAR DE ALTA EN THE THINGS NETWORKS UN DISPOSITIVO CON GENERACION ALEATRORIA DE DEVEUI Y APPEUI, DESPUES ESCRIBIR AQUÍ TODAS LAS CLAVES*/
/*0x0A
const char *appEui  = "70B3D57ED0041D2A";
const char *appKey  = "4FDB7D8301C8B5EDA76150010A8E32C2";
const char *devEui  = "00337988C57E1A3C";
/**/

/*0x0B*/
  const char *appEui  = "70B3D57ED0041D2A";
  const char *appKey  = "472A6E045ED9791019ABBAE4451B10D9";
  const char *devEui  = "00F91B72C6AE42ED";
  /**/

/*
   UNIONES PARA HACER ENVIOS BYTE A BYTE DE FORMA ORDENADA
   EXISTE UNION DE FLOAT A BYTE [4 BYTES] (USAR PARA LA MAYORIA DE VARIABLES)
   EXISTE UNION DE DOUBLE A BYTE [8 BYTES] (USAR EN CASO DE NH3)
*/
union f2b {
  float fbyte;
  byte bbyte[3];
};

void setup() {
  /* INITIALIZE ALL YOU NEED TO USE THIS SENSOR DEVICE */

  /* Serial Port to degub purpouse */
  Serial.begin(115200);

  /* Ammonia and Carbon Dioxide sensors */
  NH3.begin();
  CO2.begin();

  /* Temperature and Humidity sensors */
  /*
  if (bme.begin(BME280_ADDRESS) == true)
  {
    Serial.println ("BME280 is in " + String(BME280_ADDRESS));
  }
  else if (bme.begin(BME280_ADDRESS_ALTERNATE) == true)
  {
    Serial.println ("BME280 is in " + String(BME280_ADDRESS_ALTERNATE));
  }*/
   if (aht.begin(eAHT10Address_Low) == true)
  {
    Serial.println ("AHT10 is in " + String(eAHT10Address_Low));
  }
  else if (aht.begin(eAHT10Address_High) == true)
  {
    Serial.println ("AHT10 is in " + String(eAHT10Address_High));
  }
  else
  {
    Serial.println("Tmp & hum sensor was not found!");
  }

  /* LoRaWAN communications init */
  connectLoRaWAN();
}

/*PON LO QUE QUIERAS QUE SE EJECUTE CONTINUAMENTE*/
/*INICIALIZA TODO LO QUE SE VA A USAR EN EL DISPOSITIVO
  leer amoniaco
  leer dioxido de carbono
  leer temperatura humedad y presión
  mostrar los datos por puerto serie
  envio de la información mediante LoRaWAN
  Esperar sin hacer nada
*/
/*COMO POSIBLES MEJORAS
  [pending]   Que este haciendo medias durante un tiempo y despues envie los datos
  [fixed]     En el servidor poner que NH3 es de 8 bytes y no 4 bytes (The things networks y base de datos)
              versión final quitar showData
  [pending]   eliminar delay

*/
void loop() {

  /* Get sensor data */
  leerNH3();
  leerCO2();
  leerTMP();

  /* Show sensors data */
  showData();

  /* Format and send sensors data */
  sendData();

  /* Get Ro of the NH3 Sensor /** ///backspace to the space before this to uncomment to activate Ro function.
      Serial.println("El valor de Ro = " + String(calculaR0(A1, 10.00)));
      /**/

  delay(timeToSend);

}
/*
   LEER NH3
   SE PUEDEN VARIAR EL NUMERO DE MUESTRAS QUE REALIZA MEDIANTE EL DEFINE MUESTRAS
*/
void leerNH3() {
  NH3.dataNH3.ppm = 0;
  float res1 = 0;
  for (uint32_t con = 0; con < MUESTRAS; con++) {
    res1 = NH3.read();
    NH3.dataNH3.ppm = NH3.dataNH3.ppm + res1;
  }
  NH3.dataNH3.ppm = NH3.dataNH3.ppm / MUESTRAS;
}
/*
   LEER CO2
   SE PUEDEN VARIAR EL NUMERO DE MUESTRAS QUE REALIZA MEDIANTE EL DEFINE MUESTRAS
*/
void leerCO2() {
  CO2.dataCO2.volts = 0;
  float res1 = 0;
  for (uint32_t con = 0; con < MUESTRAS; con++) {
    res1 = CO2.read();
    CO2.dataCO2.volts = CO2.dataCO2.volts + res1;
  }
  CO2.dataCO2.volts = CO2.dataCO2.volts / MUESTRAS;
  CO2.dataCO2.percent = CO2.percentage(CO2.dataCO2.volts, CO2.dataCO2.curve);
}

/*PARA DEPURAR QUE LA INFORMACIÓN SE MUESTRA BIEN SE MUESTRAN TODOS LOS DATOS*/
void showData() {
  Serial.println("*****INIT SHOW DATA*****");

  if (NH3.dataNH3.ppm < 10) {
    Serial.println("NH3 < 10 ppm" );
  }
  else if (NH3.dataNH3.ppm > 200) {
    Serial.println("NH3 > 200 ppm" );
  }
  else {
    Serial.println("NH3 " + String(NH3.dataNH3.ppm) + " ppm" );
  }
  if (CO2.dataCO2.percent < 400) {
    Serial.println("CO2 < 400 ppm" );
  }
  else if (CO2.dataCO2.percent > 10000) {
    Serial.println("CO2 > 10000 ppm" );
  }
  else {
    Serial.println("CO2 " + String(CO2.dataCO2.percent) + " ppm" );
  }
  Serial.println("T: " + String(temp) + " ºC");
  Serial.println("RH: " + String(hum) + " %");
  Serial.println("P: " + String(pres) + " hPa");
  Serial.println("Approx Altitud: " + String(alt) + " m");
  Serial.println("******END******");
}


/*CONECTAR MEDIANTE OTAA CON LAS CLAVES APPEUI, APPKEY Y EL DEVEUI*/
void connectLoRaWAN () {
  LoRaWAN.begin(BANDA);
  LoRaWAN.addChannel(1, 868300000, 0, 6);
  LoRaWAN.joinOTAA(appEui, appKey, devEui);
}
/*MANDAR MEDIANTE LORAWAN LOS SIGUIENTES VALORES*/
/*
  SN  : IDENTIFICADOR DEL DISPOSITIVO (SE ENCUENTRA EN LOS DEFINES) QUE ES UN BYTE, ES DECIR SE MANDA UN BYTE.
  NH3 : AMONIACO QUE ES UN DOOBLE, ES DECIR SE MANDAN 8 BYTES
  CO2 : DIÓXIDO DE CARBONO QUE ES UN FLOAT, ES DECIR SE MANDAN 4 BYTES
  T   : TEMPERATURA QUE ES UN FLOAT, ES DECIR SE MANDAN 4 BYTES
  H   : HUMEDAD QUE ES UN FLOAT, ES DECIR SE MANDAN 4 BYTES
  P   : PRESION QUE ES UN FLOAT, ES DECIR SE MANDAN 4 BYTES
  A   : ALTITUD APROXIMADA QUE ES UN FLOAT, ES DECIR SE MANDAN 4 BYTES
*/

void sendData() {
  f2b n;
  f2b c;
  f2b t;
  f2b h;
  f2b p;
  f2b a;
  n.fbyte = (float)NH3.dataNH3.ppm;
  c.fbyte = CO2.dataCO2.percent;
  t.fbyte = temp;
  h.fbyte = hum;
  p.fbyte = pres;
  a.fbyte = alt;
  LoRaWAN.beginPacket();
  LoRaWAN.write(SN);
  LoRaWAN.write(n.bbyte[0]);
  LoRaWAN.write(n.bbyte[1]);
  LoRaWAN.write(n.bbyte[2]);
  LoRaWAN.write(n.bbyte[3]);
  LoRaWAN.write(c.bbyte[0]);
  LoRaWAN.write(c.bbyte[1]);
  LoRaWAN.write(c.bbyte[2]);
  LoRaWAN.write(c.bbyte[3]);
  LoRaWAN.write(t.bbyte[0]);
  LoRaWAN.write(t.bbyte[1]);
  LoRaWAN.write(t.bbyte[2]);
  LoRaWAN.write(t.bbyte[3]);
  LoRaWAN.write(h.bbyte[0]);
  LoRaWAN.write(h.bbyte[1]);
  LoRaWAN.write(h.bbyte[2]);
  LoRaWAN.write(h.bbyte[3]);
  LoRaWAN.write(p.bbyte[0]);
  LoRaWAN.write(p.bbyte[1]);
  LoRaWAN.write(p.bbyte[2]);
  LoRaWAN.write(p.bbyte[3]);
  LoRaWAN.write(a.bbyte[0]);
  LoRaWAN.write(a.bbyte[1]);
  LoRaWAN.write(a.bbyte[2]);
  LoRaWAN.write(a.bbyte[3]);
  LoRaWAN.endPacket();
  /*  else {
      connectLoRaWAN();
      sendData();
    }*/
}

/*
    this function is created to select reading function of tmp & hum
    sensor.
*/
void leerTMP() {
  /*
     leerBME(); --> read with BME280
     leerAHT(); --> read with AHT10
     ...
  */

  leerAHT();
}

/*
   LEER TEMPERATURA, PRESIÓN, HUMEDAD Y ALTITUD DEL SENSOR BME280
*//*

void leerBME() {
  temp = bme.readTemperature();
  pres = bme.readPressure() / 100.0F;
  hum = bme.readHumidity();
  alt = bme.readAltitude(SEALEVELPRESSURE_HPA);
}
*/

/*
   LEER TEMPERATURA Y HUMEDAD AHT10/**/

  void leerAHT()
  {
  hum = aht.GetHumidity();
  temp = aht.GetTemperature();
  }
/**/

/* Para dejar comentado otra vez */

float calculaR0(uint8_t pin, float Rl) {
  float R0_res = 0;
  float res = 0;
  int val = 0;
  float Rs_air = 0;
  float R0 = 0;
  for (uint32_t x = 0 ; x < 10 ; x++) {
    val = analogRead(pin);
    res = (float)val * (3.307 / 1024);
    Rs_air = ((3.307 * Rl) / res) - Rl;
    R0 = Rs_air / 3.6;
  }
  return R0;
}
