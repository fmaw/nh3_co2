/*!
   @file AHT10.cpp

   @mainpage AHT10 humidity, temperature

   @section intro_sec Introduction

   Driver for the AHT10 humidity, temperature

   These sensors use I2C

   @section author fmawic@gmail.com

   Written by Fran "fmawic" M. Alvarez Wic for Sensoryca.

   @section license BSD

   BSD license, all text here must be included in any redistribution.
   See the LICENSE file for details.

*/
#include "AHT10.h"
#include <Wire.h>
#include <Arduino.h>
#include <stdint.h>
#include <math.h>

// Specify the constants for water vapor and barometric pressure.
#define WATER_VAPOR 17.62f
#define BAROMETRIC_PRESSURE 243.5f

#define AHT10_I2CADDR_DEFAULT 0x38   ///< AHT10 default i2c address
#define AHT10_CMD_CALIBRATE 0xE1     ///< Calibration command
#define AHT10_CMD_TRIGGER 0xAC       ///< Trigger reading command
#define AHT10_CMD_SOFTRESET 0xBA     ///< Soft reset command
#define AHT10_STATUS_BUSY 0x80       ///< Status bit for busy
#define AHT10_STATUS_CALIBRATED 0x08 ///< Status bit for calibrated

uint8_t eSensorCalibrateCmd[3] = {AHT10_CMD_CALIBRATE, AHT10_STATUS_CALIBRATED, 0x00};
uint8_t eSensorNormalCmd[3]    = {0xA8, 0x00, 0x00};
uint8_t eSensorMeasureCmd[3]   = {AHT10_CMD_TRIGGER, 0x33, 0x00};
uint8_t eSensorResetCmd        = AHT10_CMD_SOFTRESET;
bool    GetRHumidityCmd        = true;
bool    GetTempCmd             = false;

uint8_t status;
uint8_t error_code;

/******************************************************************************
   Global Functions
 ******************************************************************************/
AHT10Class::AHT10Class()
{
  status = true;
  error_code = false;
}

bool AHT10Class::begin(unsigned char _AHT10_address)
{
  uint8_t tmp = 0x00;
  delay (20); // 20 ms to power up the sensor (max 20 ms in datasheet)
  /* Set address */
  AHT10_address = _AHT10_address;

  /* Init the IIC bus    */
  Wire.begin();
  Wire.beginTransmission(AHT10_address);
  /* Reset de device */
  Wire.write(eSensorResetCmd);
  /* Wait to reboot device */
  delay(20);
  /* Init write and calibrate device */
  Wire.write(eSensorCalibrateCmd, 3);


  delay(20);
  Wire.requestFrom(AHT10_address, 1);
  tmp = Wire.read();
  Wire.endTransmission();
  
  if ((tmp & 0x68) == 0x08)
    return true;
  else
  {
    return false;
  }

}


/**********************************************************
   GetHumidity
    Gets the current humidity from the sensor.

   @return float - The relative humidity in %RH
 **********************************************************/
float AHT10Class::GetHumidity(void)
{
  float value = readSensor(GetRHumidityCmd);
  if (value == 0) {
    return 0;                       // Some unrealistic value
  }
  return value * 100 / 1048576;
}

/**********************************************************
   GetTemperature
    Gets the current temperature from the sensor.

   @return float - The temperature in Deg C
 **********************************************************/
float AHT10Class::GetTemperature(void)
{
  float value = readSensor(GetTempCmd);
  return ((200 * value) / 1048576) - 50;
}

/**********************************************************
   GetDewPoint
    Gets the current dew point based on the current humidity and temperature

   @return float - The dew point in Deg C
 **********************************************************/
float AHT10Class::GetDewPoint(void)
{
  float humidity = GetHumidity();
  float temperature = GetTemperature();

  // Calculate the intermediate value 'gamma'
  float gamma = log(humidity / 100) + WATER_VAPOR * temperature / (BAROMETRIC_PRESSURE + temperature);
  // Calculate dew point in Celsius
  float dewPoint = BAROMETRIC_PRESSURE * gamma / (WATER_VAPOR - gamma);

  return dewPoint;
}



/******************************************************************************
   Private Functions
 ******************************************************************************/

unsigned long AHT10Class::readSensor(bool GetDataCmd)
{
  unsigned long result, temp[6];

  Wire.beginTransmission(AHT10_address);
  Wire.write(eSensorMeasureCmd, 3);
  Wire.endTransmission();
  delay(100);

  Wire.requestFrom(AHT10_address, 6);

  for (unsigned char i = 0; Wire.available() > 0; i++)
  {
    temp[i] = Wire.read();
  }

  if (GetDataCmd)
  {
    result = ((temp[1] << 16) | (temp[2] << 8) | temp[3]) >> 4;
  }
  else
  {
    result = ((temp[3] & 0x0F) << 16) | (temp[4] << 8) | temp[5];
  }

  return result;
}

unsigned char AHT10Class::readStatus(void)
{
  unsigned char result = 0;

  Wire.requestFrom(AHT10_address, 1);
  result = Wire.read();
  return result;
}

void AHT10Class::Reset(void)
{
  Wire.beginTransmission(AHT10_address);
  Wire.write(eSensorResetCmd);
  Wire.endTransmission();
  delay(20);
}
